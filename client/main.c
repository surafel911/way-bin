#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include <wayland-client.h>
#include <xdg-shell-client-protocol.h>

const int32_t RGBA_SIZE = 4;

struct _wl_client_globals {
    struct wl_display* display;
    struct wl_registry* registry;
    struct wl_registry_listener listener;
    struct wl_compositor* compositor;
    struct wl_shm* shared_memory;
    struct wl_buffer* buffer;
    struct wl_surface* surface;
    struct wl_callback* surface_frame_callback;
    struct wl_callback_listener surface_frame_callback_listener;
    uint8_t* pixels;
    bool close;
    uint32_t width;
    uint32_t height;
};

// Forward declarations for the listener in _wl_client_globals

static void
_wl_registry_handle_global(void*, struct wl_registry*, uint32_t, const char*, uint32_t);

static void
_wl_registry_handle_global_remove(void*, struct wl_registry*, uint32_t);

static void
_wl_surface_new_frame_callback(void*, struct wl_callback*, uint32_t);

struct _wl_client_globals _wl_client_globals = { 
    .display = NULL,
    .registry = NULL,
    .listener = {
        .global = _wl_registry_handle_global,
        .global_remove = _wl_registry_handle_global_remove
    },
    .compositor = NULL,
    .shared_memory = NULL,
    .buffer = NULL,
    .surface = NULL,
    .surface_frame_callback = NULL,
    .surface_frame_callback_listener = {
        .done = _wl_surface_new_frame_callback,
    },
    .pixels = NULL,
    .close = false,
    .width = 1024,
    .height = 720,
};

static void
_xdg_surface_configure(void* data, struct xdg_surface* surface, uint32_t serial);

static void
_xdg_toplevel_configure(void* data, struct xdg_toplevel* top, int32_t w, int32_t h, struct wl_array* states);

static void
_xdg_toplevel_close(void* data, struct xdg_toplevel* top);

static void
_xdg_wm_base_ping(void* data, struct xdg_wm_base* wm_base, uint32_t serial);

struct _xdg_client_globals {
    struct xdg_wm_base* wm_base;
    struct xdg_wm_base_listener wm_base_listener;
    struct xdg_surface* surface;
    struct xdg_surface_listener surface_listener;
    struct xdg_toplevel* toplevel;
    struct xdg_toplevel_listener toplevel_listener;
};

struct _xdg_client_globals _xdg_client_globals = {
    .wm_base = NULL,
    .wm_base_listener = {
        .ping = &_xdg_wm_base_ping,
    },
    .surface = NULL,
    .surface_listener =  {
        .configure = _xdg_surface_configure,
    },
    .toplevel = NULL,
    .toplevel_listener = {
        .configure = _xdg_toplevel_configure,
        .close = _xdg_toplevel_close,
    },
};

static const int32_t
_create_shared_fd(size_t size) {
    char name[8];
    memset(name, 0, sizeof(name));

    name[0] = '/';
    name[7] = '\0';
    for (uint8_t i = 1; i < 6; ++i) {
        name[i] = (rand() & 23) + 97;
    }

    int32_t fd = shm_open(name, O_RDWR | O_CREAT | O_EXCL, S_IRUSR |  S_IWUSR | S_IROTH | S_IWOTH);

    shm_unlink(name);
    ftruncate(fd, size);

    return fd;
}

static void
_resize_window() {
    uint32_t SIZE = _wl_client_globals.width * _wl_client_globals.height * RGBA_SIZE;

    int32_t fd = _create_shared_fd(SIZE);
    _wl_client_globals.pixels = mmap(NULL, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    
    struct wl_shm_pool* pool = wl_shm_create_pool(_wl_client_globals.shared_memory, fd, SIZE);
    _wl_client_globals.buffer = wl_shm_pool_create_buffer(pool, 0, _wl_client_globals.width, _wl_client_globals.height, _wl_client_globals.width * RGBA_SIZE, WL_SHM_FORMAT_ARGB8888);

    // wl_shm_pool_destroy(pool);
    close(fd);
}

uint8_t c = 0;
static void
_draw_window() {
    c++;
    memset(_wl_client_globals.pixels, c, _wl_client_globals.width * _wl_client_globals.height * RGBA_SIZE);

    wl_surface_attach(_wl_client_globals.surface, _wl_client_globals.buffer, 0, 0);
    wl_surface_damage_buffer(_wl_client_globals.surface, 0, 0, _wl_client_globals.width, _wl_client_globals.height);
    wl_surface_commit(_wl_client_globals.surface);
}

// Notifies client of global objects shared b/w client and 
// server
static void
_wl_registry_handle_global(
    void* data,
    struct wl_registry* registry,
    uint32_t name,
    const char* interface,
    uint32_t version) {
    if (strcmp(interface, wl_compositor_interface.name) == 0) {
        _wl_client_globals.compositor = wl_registry_bind(
            registry,
            name,
            &wl_compositor_interface,
            4);
    } else if (strcmp(interface, wl_shm_interface.name) == 0) {
        _wl_client_globals.shared_memory = wl_registry_bind(
            registry,
            name,
            &wl_shm_interface,
            1);
    } else if (strcmp(interface, xdg_wm_base_interface.name) == 0) {
        _xdg_client_globals.wm_base = wl_registry_bind(
            registry,
            name,
            &xdg_wm_base_interface,
            1);

        xdg_wm_base_add_listener(_xdg_client_globals.wm_base, &_xdg_client_globals.wm_base_listener, NULL);
    }
}

// Notifies client when global objects are removed from share state
static void
_wl_registry_handle_global_remove(void* data, struct wl_registry* registry, uint32_t name) {

}

static void
_wl_surface_new_frame_callback(void* data, struct wl_callback* callback, uint32_t wl_callback_get_user_data) {
    // what the actual fuck is this tutorial doing with this
    wl_callback_destroy(callback);

    callback = wl_surface_frame(_wl_client_globals.surface);
    wl_callback_add_listener(callback, &_wl_client_globals.surface_frame_callback_listener, NULL);

    _draw_window();
}

static void
_xdg_surface_configure(void* data, struct xdg_surface* surface, uint32_t serial) {
    xdg_surface_ack_configure(surface, serial);

    if (!_wl_client_globals.pixels) {
        _resize_window();
    }

    _draw_window();
}

static void
_xdg_toplevel_configure(void* data, struct xdg_toplevel* top, int32_t width, int32_t height, struct wl_array* states) {
    if (!width && !height) {
        return;
    }

    if (_wl_client_globals.width != width || _wl_client_globals.height != height) {
        munmap(_wl_client_globals.pixels, _wl_client_globals.width * _wl_client_globals.height * RGBA_SIZE);

        _wl_client_globals.width = width;
        _wl_client_globals.height = height;
        _resize_window();
    }
}

static void
_xdg_toplevel_close(void* data, struct xdg_toplevel* top) {
    _wl_client_globals.close = true;
}

static void
_xdg_wm_base_ping(void* data, struct xdg_wm_base* wm_base, uint32_t serial) {
    xdg_wm_base_pong(_xdg_client_globals.wm_base, serial);
}

static void
_way_bin_close_client() {
    if (!_xdg_client_globals.toplevel) {
        xdg_toplevel_destroy(_xdg_client_globals.toplevel);
    }

    fprintf(stdout, "Destroyed xdg top level\n");

    if (!_xdg_client_globals.surface) {
	    xdg_surface_destroy(_xdg_client_globals.surface);
    }
    
    fprintf(stdout, "Destroyed xdg surface\n");

    if (!_wl_client_globals.surface) {
	    wl_surface_destroy(_wl_client_globals.surface);
    }

    fprintf(stdout, "Destroyed wayland surface\n");

    // Clean up wayland buffer
    if (_wl_client_globals.buffer != NULL) {
        wl_buffer_destroy(_wl_client_globals.buffer);
    }

    fprintf(stdout, "Destroyed wayland buffer\n");

    // Destroy registry
    if (_wl_client_globals.registry) {
        wl_registry_destroy(_wl_client_globals.registry);
    }
    
    fprintf(stdout, "Destroyed wayland registry\n");

    // Close display connection
    if (_wl_client_globals.display) {
        wl_display_disconnect(_wl_client_globals.display);
    }
    
    fprintf(stdout, "Closed wayland server connection\n");
}

static int
_way_bin_start_client() {
    // Looks for active wayland server based on environment
    // variables
    _wl_client_globals.display = wl_display_connect(NULL);
    if (!_wl_client_globals.display) {
        fprintf(stderr, "Failed to connect to wayland server...\n");
        return -1;
    }

    fprintf(stdout, "Successfully connected to wayland display\n");

    // Registry acts as mailbox for state changes b/w client and
    // server
    _wl_client_globals.registry = wl_display_get_registry(_wl_client_globals.display);
    if (!_wl_client_globals.registry) {
        fprintf(stderr, "Failed to get registry for wayland display...\n");
        return -1;
    }

    fprintf(stdout, "Successfully acquired display registry\n");

    // Assign listener to registry; callbacks are how client
    // interacts with server
    if (wl_registry_add_listener(_wl_client_globals.registry, &_wl_client_globals.listener, NULL)) {
        fprintf(stderr, "Failed to add wayland listener to registry...\n");
        return -1;
    }
    
    fprintf(stdout, "Successfully added wayland listener to registry\n");

    // Client is single and ready to mingle (with some data)
    if (wl_display_roundtrip(_wl_client_globals.display) == -1) {
        fprintf(stderr, "Failed wayland display roundtrip...\n");
        return -1;
    }

    fprintf(stdout, "Successfully completed display roundtrip\n");

    _wl_client_globals.surface = wl_compositor_create_surface(_wl_client_globals.compositor);
    if (!_wl_client_globals.surface) {
        fprintf(stderr, "Failed to create wayland surface...\n");
        return -1;
    }

    fprintf(stdout, "Successfully created wayland surface\n");

    _wl_client_globals.surface_frame_callback = wl_surface_frame(_wl_client_globals.surface);
    if (!_wl_client_globals.surface_frame_callback) {
        fprintf(stderr, "Failed to create wayland surface frame callback...\n");
        return -1;
    }
        
    fprintf(stdout, "Successfully created wayland surface frame callback\n");

    if (wl_callback_add_listener(_wl_client_globals.surface_frame_callback, &_wl_client_globals.surface_frame_callback_listener, NULL)) {
        fprintf(stderr, "Failed to add wayland surface frame callback to listener...\n");
        return -1;
    }

    fprintf(stdout, "Successfully add wayland surface frame callback to listener\n");

    _xdg_client_globals.surface = xdg_wm_base_get_xdg_surface(_xdg_client_globals.wm_base, _wl_client_globals.surface);
    if (!_xdg_client_globals.surface) {
        fprintf(stderr, "Failed to create xdg surface...\n");
        return -1;
    }

    fprintf(stdout, "Successfully created xdg surface\n");

    if (xdg_surface_add_listener(_xdg_client_globals.surface, &_xdg_client_globals.surface_listener, NULL)) {
        fprintf(stderr, "Failed to add xdg surface listener...\n");
        return -1;
    }
    
    fprintf(stdout, "Successfully added xdg surface listener\n");

    _xdg_client_globals.toplevel = xdg_surface_get_toplevel(_xdg_client_globals.surface);
    if (!_xdg_client_globals.toplevel) {
        fprintf(stderr, "Failed to get top level from xdg surface...\n");
        return -1;
    }

    fprintf(stdout, "Successfully acquired the top level from xdg surface\n");

    if (xdg_toplevel_add_listener(_xdg_client_globals.toplevel, &_xdg_client_globals.toplevel_listener, NULL)) {
        fprintf(stderr, "Failed to add xdg top level listener...\n");
    }

    fprintf(stdout, "Successfully added xdg top level listener\n");

    xdg_toplevel_set_title(_xdg_client_globals.toplevel, "wayland client");
    wl_surface_commit(_wl_client_globals.surface);

    return 0;
}

int
main() {
    if (_way_bin_start_client()) {
        _way_bin_close_client();
    }

    while (wl_display_dispatch(_wl_client_globals.display)) {
        if (_wl_client_globals.close) break;
    }

    _way_bin_close_client();

    return 0;
}